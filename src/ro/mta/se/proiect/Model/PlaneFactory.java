package ro.mta.se.proiect.Model;

/**
 * Created by Arkwright on 2/5/2017.
 */
public class PlaneFactory {
    public Plane getPlane(String planeType)
    {
        if(planeType == null)
        {
            return null;
        }
        if(planeType.equals("FighterPlane"))
        {
            return new FighterPlane();
        }
        return  null;
    }
}
