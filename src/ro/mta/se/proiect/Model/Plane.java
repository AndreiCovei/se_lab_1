package ro.mta.se.proiect.Model;

import javafx.scene.Group;

/**
 * Created by Arkwright on 1/31/2017.
 */
public interface Plane {
    void draw(Group root);
}
