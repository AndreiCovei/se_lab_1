package ro.mta.se.proiect.Model;

import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;

/**
 * Created by Arkwright on 2/5/2017.
 */
public class FighterPlane implements Plane {
    public static int noOfPlanes;

    public FighterPlane(){
        noOfPlanes++;

    }

    @Override
    public void draw(Group root) {
        int xPos = 320;
        int yPos = 250;
        System.out.println(noOfPlanes);
        Image planeImage = new Image("ro/mta/se/proiect/View/avion.png");
        ImageView imgView = new ImageView(planeImage);
        imgView.setX(320);
        imgView.setY( 250 - ( ( noOfPlanes - 1 ) * 120 ) );
        root.getChildren().add(imgView);

        imgView.setOnDragDetected(new EventHandler<MouseEvent>() {
            public void handle(MouseEvent event)
            {

                Dragboard db = imgView.startDragAndDrop(TransferMode.ANY);
                ClipboardContent content = new ClipboardContent();
                content.putString("bla"); //trebuie aici un string
                //source.setCursor(Cursor.CLOSED_HAND);

                Image image = new Image("ro/mta/se/proiect/View/avion.png");
                db.setDragView(image, 5 ,5);
                db.setContent(content);
                event.consume();
            }
        });
    }
}
