package ro.mta.se.proiect;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * Created by Arkwright on 1/2/2017.
 */
public class Strings {
    private static final String DEFAULT_LANGUAGE = "en";

    private static List<String> knownLanguages = Arrays.asList("en", "ro");

    private static Map<String, String> stringMap;

    private Strings(String language) {
        if (language != null) {
            switch (language) {
                case "ro": {
                    loadRomanianStrings();
                    break;
                }
                default: {
                    loadEnglishStrings();
                    break;
                }
            }
        }
    }

    private void loadEnglishStrings() {
        stringMap = new HashMap<>();

        stringMap.put("windowTitle", "Hello!");
    }

    private void loadRomanianStrings() {
        stringMap = new HashMap<>();

        stringMap.put("windowTitle", "Neata!");
    }

    public static void setLanguage(String language) {
        if (language != null && knownLanguages.contains(language.toLowerCase())) {
            // Init strings according to language
            new Strings(language.toLowerCase());
        } else {
            System.out.println("Strings: Unknown language, using default");
        }
    }

    public static String get(String keyValue) {
        if (stringMap == null) {
            // Init strings with default language
            new Strings(DEFAULT_LANGUAGE);
        }

        if (stringMap != null && stringMap.containsKey(keyValue)) {
            return stringMap.get(keyValue);
        }

        return null;
    }
}
