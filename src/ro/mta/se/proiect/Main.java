package ro.mta.se.proiect;
import com.sun.xml.internal.ws.dump.LoggingDumpTube;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import ro.mta.se.proiect.Model.GameFacade;


/**
 * Created by Arkwright on 1/2/2017.
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override

    public void start(Stage primaryStage) {

        primaryStage.setTitle(Strings.get("windowTitle"));
        GameFacade gf = GameFacade.getInstance();
        primaryStage.setScene(new Scene(gf.root, 800, 600));
        primaryStage.show();

    }


}
