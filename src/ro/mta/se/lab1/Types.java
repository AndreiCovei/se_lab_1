package ro.mta.se.lab1;

/**
 * Created by Arkwright on 11/20/2016.
 */
public enum Types {
    Carnivore, Herbivore, Omnivorous
}
