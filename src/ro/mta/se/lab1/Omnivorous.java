package ro.mta.se.lab1;

/**
 * Created by Arkwright on 11/20/2016.
 */
public class Omnivorous implements Animal {
    String Nume;
    int Cotet;
    OmnivorousFood hrana = new OmnivorousFood();

    public Omnivorous(String name, int cotet, double cantitate, long timp) {
        Nume = name;
        Cotet = cotet;
        this.hrana.setQuantity(cantitate);
        this.hrana.setTime(timp);
    }

    public String getNume() {
        return Nume;
    }

    public void setNume(String nume) {
        Nume = nume;
    }

    public int getCotet() {
        return Cotet;
    }

    public void setCotet(int cotet) {
        Cotet = cotet;
    }

    public void eat() {
        System.out.print("I eat meat and herbs");
        long currently = System.currentTimeMillis() / 1000;

    }

    ;
}
