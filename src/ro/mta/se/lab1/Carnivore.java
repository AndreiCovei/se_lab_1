package ro.mta.se.lab1;

/**
 * Created by Arkwright on 11/20/2016.
 */
public class Carnivore implements Animal {
    String Nume;
    int Cotet;
    CarnivoreFood hrana = new CarnivoreFood();

    public Carnivore(String name, int cotet, double cantitate, long timp) {
        Nume = name;
        Cotet = cotet;
        this.hrana.setQuantity(cantitate);
        this.hrana.setTime(timp);
    }

    public String getNume() {
        return Nume;
    }

    public void setNume(String nume) {
        Nume = nume;
    }

    public int getCotet() {
        return Cotet;
    }

    public void setCotet(int cotet) {
        this.Cotet = cotet;
    }

    public void eat() {
        System.out.print("I eat meat");
        long currently = System.currentTimeMillis() / 1000;
    }

    ;
}

