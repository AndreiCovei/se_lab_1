package ro.mta.se.lab1;

/**
 * Created by Arkwright on 11/20/2016.
 */
public interface Animal {
    public void eat();
}
