package ro.mta.se.lab1;

/**
 * Created by Arkwright on 11/20/2016.
 */
public abstract class Food {
    public double Quantity = 0;
    public long time;

    public double getQuantity() {
        return Quantity;
    }

    public void setQuantity(double quantity) {
        Quantity = quantity;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }
}

class CarnivoreFood extends Food {

}

class HerbivorFood extends Food {

}

class OmnivorousFood extends Food {

}