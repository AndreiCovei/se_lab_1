package ro.mta.se.lab5;

import java.util.concurrent.TimeUnit;

/**
 * Created by Arkwright on 11/23/2016.
 */

public class Timer {
    private TimeObserver[] observers = new TimeObserver[3];
    private int totalObs = 0;
    private String state;
    public String time;
    private static Timer currentInstance = new Timer();

    public static Timer getInstance() {
        return currentInstance;
    }

    private Timer() {
        state = "on";
        new Thread(new Runnable() {
            @Override
            public void run() {
                while ( state!="off" ) { // cat timp este state ON se updateaza observatorii
                    for (int i = 0; i < totalObs; i++) {
                        observers[i].update();
                    }
                    try {
                        Thread.sleep(950);
                    } catch (InterruptedException e) {
                        System.out.println("Could not sleep thread " + e.getMessage());
                    }
                }
            }
        }).start();
    }

    public void stop() {
        this.state = "off";
    }

    public void attach(TimeObserver observer) {

        observers[totalObs++] = observer;
    }

    public void detach() {
        observers[totalObs] = null; // se decupleaza un observator
        totalObs--;

    }
}
