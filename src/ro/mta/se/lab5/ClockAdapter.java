package ro.mta.se.lab5;

/**
 * Created by Arkwright on 11/25/2016.
 */

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;

public class ClockAdapter {

    private Hours hour;
    private Minutes minute;
    private Seconds second;
    private Timer t;
    private String format;
    private String time;
    public Boolean State = true;

    public ClockAdapter( String format ) {
        t = Timer.getInstance();
        hour = new Hours(t);
        minute = new Minutes(t);
        second = new Seconds(t);
        IsKeyPressed keyPress = new IsKeyPressed();
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {

            @Override
            //se verifica daca am introdus enter de la tastatura
            public boolean dispatchKeyEvent(KeyEvent ke) {
                synchronized (IsKeyPressed.class) {
                    switch (ke.getID()) {
                        case KeyEvent.KEY_PRESSED:
                            if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                                keyPress.wPressed = true;
                            }
                            break;

                        case KeyEvent.KEY_RELEASED:
                            if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
                                keyPress.wPressed = false;
                            }
                            break;
                    }
                    return false;
                }
            }
        });



        new Thread(new Runnable() {
            @Override
            public void run() {
                while(keyPress.wPressed != true) { // cat timp nu s-a apasat tasta enter
                    try {
                        Thread.sleep(950);
                    } catch (InterruptedException e) {
                        System.out.println("Could not sleep thread " + e.getMessage());
                    }
                    setTime(format); // afisam formatul
                }
                try {
                    Thread.sleep(950);
                } catch (InterruptedException e) {
                    System.out.println("Could not sleep thread " + e.getMessage());
                }
                stopTime();
            }
        }).start();
    }

    private void setTime(String format ) {
        if(format.equals("hh:mm:ss")){
            time = hour.hours + " : " + minute.minutes + " : " + second.seconds  ;
        }
        else if(format.equals("hh:mm")){
            time = hour.hours + " : " + minute.minutes;
        }
    }

    public String getTime() {
        time+='\n'; // am pus \r, dar nu imi afisa nimic in consola
        return time;
    }

    public void stopTime() {
        t.stop();
        State = false;
    }



}
