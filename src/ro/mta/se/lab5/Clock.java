package ro.mta.se.lab5;

/**
 * Created by Arkwright on 11/24/2016.
 */
import java.lang.Runnable;
import java.lang.Thread;

public class Clock {

    //declar un ClockAdapter si un ClockProxy
    private ClockAdapter ca;
    private ClockProxy cp;
    private String format;

    public Clock(String format){
        this.format = format;
        this.cp  = new ClockProxy(format);
        if(cp.isValid) { // se verifica formatul introdus
            ca = new ClockAdapter(format); // se instantiaza un ClockAdapter
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(ca.State) {
                    try {
                        Thread.sleep(950);
                    } catch (InterruptedException e) {
                        System.out.println("Could not sleep thread " + e.getMessage());
                    }
                    displayTime();
                }

            }
        }).start();
    }

    public void displayTime() {
        String time = ca.getTime();
        System.out.print(time);
    }

}
