package ro.mta.se.lab5;

import java.util.Calendar;

/**
 * Created by Arkwright on 11/23/2016.
 */
public class Minutes implements TimeObserver {
    public Timer time;
    public int minutes;
    public Calendar cal;

    public Minutes(Timer t) {
        time = t;
        time.attach(this); //atasez un observator
    }

    public void update() {
        cal = Calendar.getInstance();
        if(cal!=null) {
            minutes = cal.get(Calendar.MINUTE);
        }
    }
}
