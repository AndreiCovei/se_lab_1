package ro.mta.se.lab5;

/**
 * Created by Arkwright on 11/23/2016.
 */



public interface TimeObserver {
    public void update();
}
