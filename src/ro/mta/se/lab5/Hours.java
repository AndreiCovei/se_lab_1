package ro.mta.se.lab5;

/**
 * Created by Arkwright on 11/23/2016.
 */

import java.util.Calendar;

public class Hours implements TimeObserver {
    public Timer time;
    public int hours;
    public Calendar cal;

    public Hours(Timer t) {
        time = t;
        time.attach(this); //atasez un observator
    }

    public void update() {
        cal = Calendar.getInstance();
        if(cal!=null) {
            hours = cal.get(Calendar.HOUR_OF_DAY);
        }
    }
}
