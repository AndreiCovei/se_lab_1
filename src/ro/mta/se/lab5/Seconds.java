package ro.mta.se.lab5;

import java.util.Calendar;

/**
 * Created by Arkwright on 11/23/2016.
 */
public class Seconds implements TimeObserver {
    public Timer time;
    public int seconds;
    public Calendar cal;

    public Seconds(Timer t) {
        time = t;
        time.attach(this); //atasez un observator
    }

    public void update() {
        cal = Calendar.getInstance(); //am nevoie sa ia mereu timpul curent, atunci cand isi face update
        if(cal!=null) {
            seconds = cal.get(Calendar.SECOND);
        }
    }
}
